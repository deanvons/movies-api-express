const { v4: uuidv4 } = require('uuid');
const { getterFactory, setterFactory, deletorFactory } = require('./db');

// Customize the Movie Creator
const movieSetterFactory = (state) => ({
    ...setterFactory,
    createMovie: data => {
        // Check if the movie already exists.
        const hasMovie = state.db.get('movies').find(movie => {
            return movie.title.toLowerCase() == data.title;
        });

        if (hasMovie) {
            return false;
        }

        // Execute the Base Creator Factory.
        return movieSetterFactory(state).create(data);
    }
});

const filterFactory = state => ({
    getByGenre: genre => {
        return getterFactory(state).getAll().filter(movie => {
            return movie.genre.join(',').toLowerCase().includes(genre.toLowerCase());
        });
    }
})

const movies = (uuid) => db => {
    const state = {
        table: 'movies',
        db,
        uuid
    };

    return Object.assign(
        {},
        getterFactory(state),
        movieSetterFactory(state),
        deletorFactory(state),
        filterFactory(state)
    )
};

module.exports = movies(uuidv4);